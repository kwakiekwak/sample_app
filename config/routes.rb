Rails.application.routes.draw do
  get 'users/new'

  root 'static_pages#home'

  get 'help' => 'static_pages#help'

  get 'about' => 'static_pages#about'
  
  get 'contact' => 'static_pages#contact'

# users
  # get 'users/index' => 'users#index', as: :users
  
  get 'signup' => 'users#new'
  # post 'users/new' => 'users#create', as: :create_user
  
  # get 'users/:id' => 'users#show', as: :user
  
  resources :users
end
